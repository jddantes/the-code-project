## Visualgo
Go to [https://visualgo.net/recursion](https://visualgo.net/recursion) and try putting "Custom Code".

## Instructional Resources, Visualizations [todo]

- Jethro's pyramid/sandwich scheme
- DFS tracing on paper (5 min or so interactive/quick exercise)
- GIFS + animations (IEEE recursion vid)
- Visualgo code/simulations