#include <bits/stdc++.h>

using namespace std;

vector<string> arr({"abc", "de", "fgh"});

int N;

string build;

void f(int index){
    if(index == N){
        cout << build << endl;
        return;
    }

    for(int j = 0; j<arr[index].size(); j++){
        build.push_back(arr[index][j]);
        f(index+1);
        build.pop_back();   
    }
}

int main(){
    N = arr.size();
    f(0);
}
