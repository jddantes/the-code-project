## Problem
Given a list of strings `arr = ["abc", "de", "fgh"]`, cycle through/print all the combinations starting from the first word to the last.
Implement `f(int index, string build)` that prints the following when called:
```
adf
adg
adh
aef
aeg
aeh
bdf
bdg
bdh
bef
beg
beh
cdf
cdg
cdh
cef
ceg
ceh
```

The call in `main()` is `f(0,"")`. Make sure that your code is generalized, i.e., if I change `arr`, your code should still run without touching anything else. See [backtracking2.cpp](backtracking2.cpp).

If you are not familiar with strings, vectors, and STL, you might want to take a look at the [intro](/cpp-stl/intro).

## Extra
Passing strings within function calls is slow. Modify your code to make `build` a global variable, and modify that instead. See [backtracking2b.cpp](backtracking2b.cpp). This should be similar to `printTabs()` in [backtracking1](/backtracking/backtracking1).