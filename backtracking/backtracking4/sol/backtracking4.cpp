#include <bits/stdc++.h>

using namespace std;

string str;

int N;

vector<int> parentBrace;
vector<int> endingBrace;
vector<vector<int>> children;

stack<int> st;

string build;

void f(int index){
    if(index == N){
        cout << build << endl;
        return;
    }

    if(str[index] == '{'){
        for(auto child : children[index]){
            f(child);
        }
    } else if(str[index] == '}'){
        f(index+1);
    } else if(str[index] == ','){
        f(endingBrace[parentBrace[index]]);
    } else {
       build.push_back(str[index]);
       f(index+1);
       build.pop_back(); 
    }
}

int main(){
    while(cin >> str){
        N = str.size();
        parentBrace.assign(N, -1);
        endingBrace.assign(N, -1);
        children.assign(N, vector<int>());

        for(int i = 0; i<N; i++){
            if(str[i] == '{'){
                parentBrace[i] = i;
                children[i].push_back(i+1);
                st.push(i);
            } else if (str[i] == '}'){
                parentBrace[i] = st.top();
                endingBrace[st.top()] = i;
                st.pop();
            } else {
                parentBrace[i] = st.empty() ? -1 : st.top();
            }

            if(str[i] == ','){
                children[st.top()].push_back(i+1);
            }
        }

        f(0);
    }
    
    return 0;
}