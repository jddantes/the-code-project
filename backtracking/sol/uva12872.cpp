// UVa 12872 
#include <bits/stdc++.h>

using namespace std;

typedef pair<int, int> pi;

int grid[30][30];
int N, M; // num rows, num cols
int tabs = -1;
void printTabs(){
    for(int t = 0; t<tabs; t++) printf("\t");
}

// width from 3 to 11, I guess odd
int can_plus(int row, int col, int n){

    int half = n/2;
    if(row - half < 0 || col - half < 0 || row + half == N || col + half == M) return 0;

    for(int i = row-half; i<=row+half; i++){
        if(!grid[i][col]) return 0;
    }
    for(int j = col-half; j<=col+half; j++){
        if(!grid[row][j]) return 0;
    }
    return 1;
}

void add_grid(int row, int col, int n, int val){
    int half = n/2;
    for(int i = row-half; i<=row+half; i++){
        grid[i][col] += val;
    }
    for(int j = col-half; j<=col+half; j++){
        if(j == col) continue;
        grid[row][j] += val;
    }
}

void print_grid(){
    for(int i = 0; i<N; i++){
        printTabs();
        for(int j = 0; j<M; j++){
            printf("%d ", grid[i][j]);
        }
        printf("\n");
    }

}

bool f(int numOn, int numFound, int row, int col){
    tabs++;
    if(col >= M){
        tabs--;
        return f(numOn, numFound, row+1, 0);
    } 
    if(row == N){
        tabs--;
        return 0;
    }
    if(numFound > 9){
        tabs--;
        return 0;
    }

    if(grid[row][col]){
      
        for(int n = 3; n<=11; n+=2){
            if(numOn - (2*n-1) < 0) break;
            if(can_plus(row, col, n)){
                add_grid(row, col, n, -1);
                numOn -= 2*n-1;
                bool ret = false;
                if(!numOn){
                    printf("%d\n%d %d\n", numFound+1, row+1, col+1);
                    ret = true;
                } else {
                    ret = f(numOn, numFound+1, row, col+n/2+1);
                }
                numOn += 2*n-1;
                add_grid(row, col, n, 1);
                if(ret) {
                    tabs--; 
                    return true;
                }
            }
        }
    }
    bool other_ret = f(numOn, numFound, row, col+1);
    tabs--;
    return other_ret;
}

int main(){
    int cases;
    scanf("%d", &cases);

    for(int e = 0; e<cases; e++){
        scanf("%d %d", &N, &M);
        int sum = 0;
        for(int i = 0; i<N;i++){
            for(int j = 0; j<M; j++){
                int num;
                scanf("%d", &num);
                sum += num;
                grid[i][j] = num;
            }
        }

        f(sum,0,0,0);
    }

    return 0;
}