## Problem
Given a string `abc`, print all permutations of it:
```
abc
acb
bac
bca
cab
cba
```
Again, your code should be generalized&mdash;it should work even if we change the input.

**Hint**: Use a boolean array `taken`.

## Extra
After doing that with a boolean array, try implementing it with [bitsets](http://www.cplusplus.com/reference/bitset/bitset/?kw=bitset). Simply put, bitsets are just like boolean arrays, but are smaller and thus can be passed between functions. See [backtracking3b.cpp](backtracking3b.cpp). This will be useful later on when solving [DP](/dynamic-programming) problems. Alternatively, we can simply use an integer mask, as shown in [backtracking3c.cpp](backtracking3c.cpp).