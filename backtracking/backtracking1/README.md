## Problem
Create a function `f(n)` that prints from n, n-1, ..., 2, 1, 0, then back to 1, 2, ..., n-1, n. **No loops and global variables**.

Example. Calling `f(5)` in `main()` should show:
```
5
4
3
2
1
0
1
2
3
4
5
```

See [backtracking1.cpp](backtracking1.cpp).

**Hint 1**: Recursion is also considered as a stack. Imagine `f(5)` as the base of the stack. When you go to `f(4)` from `f(5)`, imagine `f(4)` being placed on top of the stack. When `f(4)` calls `f(3)`, `f(3)` gets placed on top of the stack, and it calls `f(2)`, and so on. When `f(3)` finishes executing, it is removed from the stack, going back to `f(4)`. All the variables of `f(4)` before calling `f(3)` remain the same. Similarly, when `f(4)` is done (gets removed) from the stack, we only have `f(5)` at the base, with its own set of variables intact.

**Hint 2**: Code the part from n to 0 first. After that, you only need one line to do the other half. See hint 1.

## Extra
After doing that, add in a `printTabs()` function to better see the output as a recursion stack. You may use a global variable `tabs` for this.

Example. Calling `f(5)` in `main()` should show:
```
5
    4
        3
            2
                1
                    0
                1
            2
        3
    4
5
```

*Extra note:* For those who have coded functions (esp. recursions) in assembly before, you might remember that we save the registers that we'll be changing using a stack (prologue) and then restore them from the stack (epilogue) just before returning to the caller. This is the exact same purpose! We want to preserve each function's address space (i.e., set of variables) so that we can jump freely between (whether same or different) function calls.


**TODO**: GIF/Animation for this. If you can do this and would like to help out, send me an e-mail at [codeproject.contact@gmail.com](mailto:codeproject.contact@gmail.com).