#include <bits/stdc++.h>

using namespace std;

int tabs = -1;
void printTabs(){
    for(int t = 0; t<tabs; t++){
        printf("\t");
    }
}

void f(int n){
    tabs++;
    if(n == 0){
        printTabs(); printf("%d\n", n);
        tabs--;
        return;
    }
    printTabs(); printf("%d\n", n);
    f(n-1);
    printTabs(); printf("%d\n", n);
    tabs--;
}

int main(){
    f(5);  // 5 4 3 2 1 0 1 2 3 4 5
    return 0;
}
