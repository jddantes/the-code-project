## Exercises, articles, and resources for learning to code.

If you totally don't know how to code (i.e. don't know about variables, if-else statements, loops, arrays, bitwise operations, functions, and pointers), then I suggest that you pick up a programming language to start with. I'd recommend Python as it is very clean and intuitive, and is also useful for a lot of applications (e.g. [developing GUI's](https://wiki.python.org/moin/GuiProgramming), [web development](https://www.djangoproject.com/), [machine learning](http://scikit-learn.org/stable/auto_examples/classification/plot_digits_classification.html), [computer vision](http://docs.opencv.org/3.0-beta/doc/py_tutorials/py_tutorials.html), [networking/security](https://www.amazon.com/Violent-Python-Cookbook-Penetration-Engineers/dp/1597499579)). If you already know something like C/C++, I'd still suggest familiarizing with it, again as it is vey useful for a lot of things. I've listed a couple of resources below to help you get started. After familiarizing yourself with syntax, I suggest you try out the exercises in the [arrays-loops](arrays-loops) folder.

If you know how to code, then you might have come across dynamic programming but struggled to grasp the concept. If so, then I suggest looking at the [dynamic-programming](dynamic-programming) folder. More importantly, make sure that you know backtracking first (unfortunately, this is something that is not emphasized or even taught in schools). If not, then check out the [backtracking](backtracking) folder.

Finally, I suggest getting into [competitive programming](competitive-programming). Programming contests are simply contests where you solve problems like [this one](https://open.kattis.com/problems/floodingfields), code the solution (usually C/C++, Java, or Python) and submit. Your code should give the **correct** output for **any given input** and should **run within the time limit**. Studying for competitive programming greatly increases your coding skill (as you explore more topics not included in a college curriculum, and more importantly you get to code the algorithms and data structures repeatedly), and will help you prepare for the technical interviews at companies like Google, Facebook, Samsung, Kalibrr, etc. The big contests offer great prizes (e.g. cash prizes, laptops, conferences), and the top coders usually get recruited by tech companies. To get started, create an account at [UVa](http://uva.onlinejudge.org/) and track your progress through [uhunt.felix-halim.net/u/\<your_username\>](http://uhunt.felix-halim.net). More resources are posted below.

It also helps to pick up [C/C++](cpp-stl). C is a fundamental language to learn (used in microcontrollers, operating systems/kernel, and other low-level applications), and C++ is an improvement, giving you classes and the Standard Template Library (STL) which gives you access to things like [resizable arrays](http://www.cplusplus.com/reference/vector/vector/?kw=vector), as well as other abstract data types like queues, stacks, priority queues, sets, and maps. This is also handy in programming contests, as Java is slower (around 10x in my experience), and Python even more so (usually programming contests have solutions in C, C++, or Java, but would be too slow for Python). Java also has those similar data structures, as well as BigInteger, but other than that I personally don't like it (syntax is too long/clunky), though it is widely used (e.g. Android, lots of corporate jobs) so you might want to look into it if you have time. I've put in some sample [C++/STL](cpp-stl) code to help you get up and running.


## Resources 

- [/r/learnprogramming](https://www.reddit.com/r/learnprogramming/). Look at the FAQ's and recommended resources at the side.
- [/r/learnpython](https://www.reddit.com/r/learnpython/). Again look at the FAQ's and learning resources.
- [Python OOP Series](https://www.reddit.com/r/learnpython/comments/5jgbiq/python_oop_series/?st=ix0jzrlj&sh=8a8f0674)
- [learnpython.org](https://www.learnpython.org/)
- [CodeAcademy](https://www.codecademy.com/learn/python) (good to familiarize the total beginner; it has a live terminal so you can code alongside the lessons)
- [Learn Python the Hard Way](https://learnpythonthehardway.org/book/)
- [YouTube channels (from Quora)](https://www.quora.com/Are-there-good-YouTube-channels-that-teach-programming-languages/answer/Quincy-Larson)
- [Comprehensive list (from data structures and algorithms to networks, security and operating systems)](https://medium.freecodecamp.com/why-i-studied-full-time-for-8-months-for-a-google-interview-cc662ce9bb13#.bo62mwpmo) on [Github](https://github.com/jwasham/google-interview-university)
- [Best websites a programmer should visit](https://github.com/sdmg15/Best-websites-a-programmer-should-visit). Has lots of resources too!
- [The Open-Source Computer Science Degree](https://www.reddit.com/r/learnprogramming/comments/3btnh6/the_opensource_computer_science_degree/?st=iy5ktjbv&sh=45ada7e2)
- [cplusplus.com](http://cplusplus.com). Usually I just go search for the STL class I'm using (e.g. vector, stack, queue) and find the methods that I need.
- [Bo Qian's Youtube playlist for STL](https://www.youtube.com/watch?v=Vc1RyqWFbiA&list=PL5jc9xFGsL8G3y3ywuFSvOuNm3GjBwdkb). Gives a good overview/introduction to STL

## Competitive Programming
- [UVa](http://uva.onlinejudge.org). Most of the programming problems from the ACM-ICPC show up here.
- [uhunt.felix-halim.net/u/your_username](http://uhunt.felix.net). Tracker for UVa problems, and arranged per category (e.g. graphs, math, string, computational geometry). Done by Felix and Steven Halim from NUS, who also wrote a book (as well as hold classes) for competitive programming.
- [visualgo.net](http://visualgo.net). Very great visualization/interactive tool for the different data structures and algorithms. Developed by Halim.
- [HackerRank](http://hackerrank.com). Another good site to practice on per category, and also holds contests from time to time (as mentioned before, sometimes even offering prizes and jobs).
- [Kattis](http://open.kattis.com). Another good site, although problems aren't categorized by tags.
- [CodeForces](http://codeforces.com). They usually have 2-hr contests, but unfortunately at midnight in Philippine time.
- [CodeChef](http://codechef.com). They have long contests ranging for several days.
- [Project Euler](http://projecteuler.net). Math. Also mirrored on HackerRank.
- [TopCoder](http://topcoder.com). They also have great tutorials.
- [Sphere OJ](http://www.spoj.com/). Similar to UVa.
- [GeeksForGeeks](http://geeksforgeeks.org). Another site with good tutorials; you usually end up here when Googling a certain algorithm/data structure.
- [This comprehensive list again](https://github.com/jwasham/google-interview-university), as if you look at it it contains lots of data structures and algorithms, even topics like strings and computational geometry which may not be in an undergraduate curriculum but show up in competitive programming (check uHunt chapters).

If you find any bugs, formatting errors (typos, grammatical errors, broken links), or simply would like to help out (e.g. make visuals/animations!), then please reach me through [codeproject.contact@gmail.com](mailto:codeproject.contact@gmail.com)! :)
