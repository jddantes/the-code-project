## Bitwise Operations

**Bitshift (<<)**
- Shifting to the left/right multiplies/divides a number by 2.
- 2<sup>N</sup>-1 gives you N ones. (e.g. 2<sup>3</sup> - 1= 8 - 1 = 111<sub>2</sub>)

**AND (&)**
- Used for masking.
- Use with complement (`~`) to zero out a bit.

**OR (|)**
- Turning on a bit.

**XOR (\^)**
- Used for toggling bits (`xor` with 1).
- `a ^ b` gets the differing bits of `a` and `b`.
 
**Smallest ON bit**
- `x & -x`.
- Used in [Fenwick Trees](https://www.topcoder.com/community/data-science/data-science-tutorials/binary-indexed-trees/#lastdigit).

## Problems
- [LED Test (UVa 416)](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&category=24&problem=357&mosmsg=Submission+received+with+ID+18923029). Involving 7-segment displays.
- [Pizza Anyone? (UVa 565)](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=show_problem&category=24&problem=506&mosmsg=Submission+received+with+ID+18935633). You can just loop through all 2<sup>16</sup> (0 to 65535) combinations and use them as bitmasks. Note that you can add integers and characaters as mentioned [here](https://gitlab.com/jddantes/the-code-project/tree/master/arrays-loops#equivalence-of-char-and-int) (e.g. `'A'+index`).