// str2_readme.cpp
#include <bits/stdc++.h>

using namespace std;


int main(){
    string str1, str2;

    char name[] = "str2";

    while(cin >> str1 >> str2) {
        printf("Input:\n```\n");
        cout << str1 << endl << str2 << endl << "```\n";

        int ans = -1;

        for(int i = 0; i+str2.size()-1 < str1.size(); i++){
            bool good = true;
            for(int j = 0; j<str2.size(); j++){
                if(str1[i+j] == str2[j]){
                    continue;
                } else {
                    good = false;
                    break;
                }
            }
            if(good){
                ans = i;
                break;
            }
        }

        printf("Output:\n```\n");

        if(ans == -1){
            printf("NOT FOUND\n");
        } else {
            printf("%d\n", ans);
        }

        printf("```\n");

    }
    printf("See [%s.cpp](%s.cpp), [%s.in](%s.in) and [%s.output](%s.output).\n", name,name,name,name,name,name );


    return 0;
}