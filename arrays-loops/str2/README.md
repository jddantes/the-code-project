## Str 2. Manually check a string for substrings. (No using [substr](http://www.cplusplus.com/reference/string/string/substr/).)
Input: two strings, each on their own line. The second is the substring to be searched in the first.
Output the index of the substring's first occurrence in the first string, or `NOT FOUND` if it cannot be found.

Input:
```
aaaaasdf
asdf
```
Output:
```
4
```
Input:
```
DOG
OG
```
Output:
```
1
```
Input:
```
somethingsomethingsomething
thing
```
Output:
```
4
```
Input:
```
somestring
notinstring
```
Output:
```
NOT FOUND
```
See [str2.cpp](str2.cpp), [str2.in](str2.in) and [str2.output](str2.output).
