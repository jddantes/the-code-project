// mat4.cpp
#include <bits/stdc++.h>

using namespace std;

#define MAX_N 1000

int main(){
    int arr[MAX_N][MAX_N];
    int N;

    while(scanf("%d", &N)!=EOF){
        for(int i = 0; i<N; i++){
            for(int j = 0; j<N; j++){
                scanf("%d", &arr[i][j]);
            }
        }

        // Your code here

        // Print output
        for(int i = 0; i<N; i++){
            for(int j = 0; j<N; j++){
                printf("%d ", arr[i][j]);
            }
            printf("\n");
        }
    }

}