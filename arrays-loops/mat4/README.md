## Mat 4. Diagonal flip (transpose). (\\)

Input format: `N`, followed by `N` lines of `N` integers (square matrix). 

*Hint: Make use of the [swap()](http://www.cplusplus.com/reference/utility/swap/?kw=swap) function in `<utility>`.*

*Hint 2: With integer division, `N/2` would give you the lower half, whether even or odd.*

Input:
```
3
1 2 3 
4 5 6 
7 8 9 
```
Output:
```
1 4 7 
2 5 8 
3 6 9 
```
Input:
```
2
1 2 
3 4 
```
Output:
```
1 3 
2 4 
```
Input:
```
4
1 2 3 4 
5 6 7 8 
9 10 11 12 
13 14 15 16 
```
Output:
```
1 5 9 13 
2 6 10 14 
3 7 11 15 
4 8 12 16 
```
Input:
```
5
1 2 3 4 5 
6 7 8 9 10 
11 12 13 14 15 
16 17 18 19 20 
21 22 23 24 25 
```
Output:
```
1 6 11 16 21 
2 7 12 17 22 
3 8 13 18 23 
4 9 14 19 24 
5 10 15 20 25 
```
Input:
```
1
1 
```
Output:
```
1 
```
See [mat4.cpp](mat4.cpp), [mat4.in](mat4.in) and [mat4.output](mat4.output).
