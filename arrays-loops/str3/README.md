## Str 3. Check if a string is a palindrome.

Input format: each string on a line of its own.

Output either `PALINDROME` or `NOT A PALINDROME` accordingly.

*Hint: With integer division, N/2 would give you the lower half, whether even or odd.*

Input:
```
asdf
```
Output:
```
NOT A PALINDROME
```
Input:
```
notapalindrome
```
Output:
```
NOT A PALINDROME
```
Input:
```
aaa
```
Output:
```
PALINDROME
```
Input:
```
bb
```
Output:
```
PALINDROME
```
Input:
```
c
```
Output:
```
PALINDROME
```
Input:
```
deeeed
```
Output:
```
PALINDROME
```
Input:
```
atoyota
```
Output:
```
PALINDROME
```
Input:
```
agiraffe
```
Output:
```
NOT A PALINDROME
```
Input:
```
madam
```
Output:
```
PALINDROME
```
Input:
```
maddam
```
Output:
```
PALINDROME
```
Input:
```
vxyz
```
Output:
```
NOT A PALINDROME
```
Input:
```
xxyyzz
```
Output:
```
NOT A PALINDROME
```
Input:
```
xxyyx
```
Output:
```
NOT A PALINDROME
```
See [str3.cpp](str3.cpp), [str3.in](str3.in) and [str3.output](str3.output).
