// str3_sol.cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    string str;

    while(getline(cin,str)){
        // Your code here
        int N = str.size();
        bool good = true;
        for(int i =0; i<N/2; i++){
            if(str[i] == str[N-1-i]){
                continue;
            } else {
                good = false;
                break;
            }
        }

        if(good){
            printf("PALINDROME\n");
        } else {
            printf("NOT A PALINDROME\n");
        }

    }

    return 0;
}