// str3_readme.cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    string str;

    char name[] = "str3";

    while(getline(cin,str)){
        // Your code here
        int N = str.size();
        bool good = true;
        for(int i =0; i<N/2; i++){
            if(str[i] == str[N-1-i]){
                continue;
            } else {
                good = false;
                break;
            }
        }

        printf("Input:\n```\n");
        cout << str << endl;
        printf("```\n");

        printf("Output:\n```\n");

        if(good){
            printf("PALINDROME\n");
        } else {
            printf("NOT A PALINDROME\n");
        }

        printf("```\n");

    }
    printf("See [%s.cpp](%s.cpp), [%s.in](%s.in) and [%s.output](%s.output).\n", name,name,name,name,name,name );

    return 0;
}