## Loop 6. Print an NxM board with alternating horizontal stripes.

The first row would contain `'*'`, the second row `'X'`, the third `'*'`, and so on.

Input:
```
3 2
```
Output:
```
**
XX
**
```
Input:
```
4 5
```
Output:
```
*****
XXXXX
*****
XXXXX
```
Input:
```
9 3
```
Output:
```
***
XXX
***
XXX
***
XXX
***
XXX
***
```
Input:
```
1 8
```
Output:
```
********
```
Input:
```
2 2
```
Output:
```
**
XX
```

See [loop6.cpp](loop6.cpp), [loop6.cpp](loop6.in) and [loop6.output](loop6.output).




