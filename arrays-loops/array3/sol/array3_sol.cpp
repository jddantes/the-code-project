// array3_sol.cpp
#include <bits/stdc++.h>

using namespace std;

#define MAX_N 1000

int main(){
    int N;
    int arr[MAX_N];

    while(scanf("%d", &N)!=EOF){
        for(int i = 0; i<N; i++){
            scanf("%d", &arr[i]);
        }

        // Your code here
        int temp = arr[N-1];
        for(int i = N-1; i>=1; i--){
            arr[i] = arr[i-1];
        }
        arr[0] = temp;

        for(int i = 0; i<N; i++){
            printf("%d ", arr[i]);
        }
        printf("\n");
    }


    return 0;
}