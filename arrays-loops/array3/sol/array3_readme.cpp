// array3_readme.cpp
#include <bits/stdc++.h>

using namespace std;

#define MAX_N 1000

int main(){
    int N;
    int arr[MAX_N];

    char name[] = "array3";

    while(scanf("%d", &N)!=EOF){
        for(int i = 0; i<N; i++){
            scanf("%d", &arr[i]);
        }
        printf("Input:\n```\n%d\n", N);
        for(int i = 0; i<N; i++) {
            printf("%d ", arr[i]);
        }
        printf("\n```\nOutput:\n```\n");

        // Your code here
        int temp = arr[N-1];
        for(int i = N-1; i>=1; i--){
            arr[i] = arr[i-1];
        }
        arr[0] = temp;

        for(int i = 0; i<N; i++){
            printf("%d ", arr[i]);
        }
        printf("\n");
        printf("```\n");
    }

    printf("See [%s.cpp](%s.cpp), [%s.in](%s.in) and [%s.output](%s.output).\n", name, name, name, name, name, name);


    return 0;
}