## Array 3. Shift an array right.

Input format: `N`, followed by the `N` integers. 

Input:
```
3
3 1 2 
```
Output:
```
2 3 1 
```
Input:
```
5
1 2 3 4 9 
```
Output:
```
9 1 2 3 4 
```
Input:
```
6
3 10 -2 3 1 3 
```
Output:
```
3 3 10 -2 3 1 
```
See [array3.cpp](array3.cpp), [array3.in](array3.in) and [array3.output](array3.output).
