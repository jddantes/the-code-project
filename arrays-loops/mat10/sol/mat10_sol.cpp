// mat10_sol.cpp
#include <bits/stdc++.h>

using namespace std;

int grid[9][9];

int freq[10];

bool checkFreq(){
    for(int i = 1; i<=9; i++){
        if(freq[i] >= 2) return false;
    }
    return true;
}

bool checkValid(){
    // Check rows
    for(int i = 0; i<9; i++){
        memset(freq, 0, sizeof freq);
        for(int j = 0; j<9; j++){
            freq[grid[i][j]]++;
        }
        if(!checkFreq()) return false;
    }

    // Check cols
    for(int j = 0; j<9; j++){
        memset(freq, 0, sizeof freq);
        for(int i = 0; i<9; i++){
            freq[grid[i][j]]++;
        }
        if(!checkFreq()) return false;
    }

    // Check boxes
    for(int I = 0; I<9; I+=3){
        for(int J = 0; J<9; J+=3){
            memset(freq, 0, sizeof freq);
            for(int i = 0; i<3; i++){
                for(int j = 0; j<3; j++){
                    freq[grid[I+i][J+j]]++;
                }
            }
            if(!checkFreq()) return false;
        }
    }

    return true;
}

int main(){
    while(scanf("%d", &grid[0][0])!=EOF){
        for(int j = 1; j<9; j++) scanf("%d", &grid[0][j]);
        for(int i = 1; i<9; i++){
            for(int j = 0; j<9; j++){
                scanf("%d", &grid[i][j]);
            }
        }

        if(checkValid()){
            printf("VALID\n");
        } else {
            printf("INVALID\n");
        }


        
    }


    return 0;
}