// loop5_sol.cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    int N;
    while(scanf("%d", &N)!=EOF){  // Assume N is odd
        for(int i = 1; i<=N; i+=2) {
            int spaces = (N-i)/2;
            for(int j = 0; j<spaces; j++){
                printf(" ");
            }
            for(int j = 0; j<i; j++){
                printf("*");
            }
            printf("\n");
        }
        for(int i = N-2; i>=1; i-=2){
            int spaces = (N-i)/2;
            for(int j = 0; j<spaces; j++){
                printf(" ");
            }
            for(int j = 0; j<i; j++){
                printf("*");
            }
            printf("\n");
        }
    }
}