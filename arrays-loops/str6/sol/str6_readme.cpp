// str6_readme.cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    string str; 
    char name[] = "str6";
    while(getline(cin, str)){
        printf("Input:\n```\n");
        cout << str << endl;
        printf("```\n");
        // Your code here
        int N = str.size();
        char c = str[N-1];
        for(int i = N-1; i>=1; i--){
            str[i] = str[i-1];
        }
        str[0] = c;

        printf("Output:\n```\n");
        cout << str << endl;
        printf("```\n");
    }
    printf("See [%s.cpp](%s.cpp), [%s.in](%s.in) and [%s.output](%s.output).\n", name,name,name,name,name,name );

    return 0;
}