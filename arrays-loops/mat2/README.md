## Mat 2. Vertical flip.

Input format: `N` and `M`, followed by `N` lines of `M` integers.

*Hint: Make use of the [swap()](http://www.cplusplus.com/reference/utility/swap/?kw=swap) function in `<utility>`.*

*Hint 2: With integer division, `N/2` would give you the lower half, whether even or odd.*

Input:
```
3 3
1 2 3 
4 5 6 
7 8 9 
```
Output:
```
7 8 9 
4 5 6 
1 2 3 
```
Input:
```
4 4
1 2 3 4 
5 6 7 8 
9 10 11 12 
13 14 15 16 
```
Output:
```
13 14 15 16 
9 10 11 12 
5 6 7 8 
1 2 3 4 
```
Input:
```
5 5
1 2 3 4 5 
6 7 8 9 10 
11 12 13 14 15 
16 17 18 19 20 
21 22 23 24 25 
```
Output:
```
21 22 23 24 25 
16 17 18 19 20 
11 12 13 14 15 
6 7 8 9 10 
1 2 3 4 5 
```
Input:
```
2 2
1 2 
3 4 
```
Output:
```
3 4 
1 2 
```
Input:
```
2 4
1 2 3 4 
5 6 7 8 
```
Output:
```
5 6 7 8 
1 2 3 4 
```
Input:
```
1 1
1 
```
Output:
```
1 
```
Input:
```
1 8
1 2 3 4 5 6 7 8 
```
Output:
```
1 2 3 4 5 6 7 8 
```
Input:
```
8 1
1 
2 
3 
4 
5 
6 
7 
8 
```
Output:
```
8 
7 
6 
5 
4 
3 
2 
1 
```
See [mat2.cpp](mat2.cpp), [mat2.in](mat2.in) and [mat2.output](mat2.output).
