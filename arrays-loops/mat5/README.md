## Mat 5. Diagonal flip. (/)

Input format: `N`, followed by `N` lines of `N` integers (square matrix). 

*Hint: Make use of the [swap()](http://www.cplusplus.com/reference/utility/swap/?kw=swap) function in `<utility>`.*

*Hint 2: With integer division, `N/2` would give you the lower half, whether even or odd.*
Input:
```
3
1 2 3 
4 5 6 
7 8 9 
```
Output:
```
9 6 3 
8 5 2 
7 4 1 
```
Input:
```
2
1 2 
3 4 
```
Output:
```
4 2 
3 1 
```
Input:
```
4
1 2 3 4 
5 6 7 8 
9 10 11 12 
13 14 15 16 
```
Output:
```
16 12 8 4 
15 11 7 3 
14 10 6 2 
13 9 5 1 
```
Input:
```
5
1 2 3 4 5 
6 7 8 9 10 
11 12 13 14 15 
16 17 18 19 20 
21 22 23 24 25 
```
Output:
```
25 20 15 10 5 
24 19 14 9 4 
23 18 13 8 3 
22 17 12 7 2 
21 16 11 6 1 
```
Input:
```
1
1 
```
Output:
```
1 
```
See [mat5.cpp](mat5.cpp), [mat5.in](mat5.in) and [mat5.output](mat5.output).
