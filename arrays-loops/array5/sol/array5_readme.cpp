// array5_readme.cpp
#include <bits/stdc++.h>

using namespace std;

#define MAX_N 1000

int main(){
    int N;
    int arr[MAX_N];

    char name[] = "array5";

    while(scanf("%d", &N)!=EOF){
        for(int i = 0; i<N; i++){
            scanf("%d", &arr[i]);
        }
        printf("Input:\n```\n%d\n", N);
        for(int i = 0; i<N; i++) {
            printf("%d ", arr[i]);
        }
        printf("\n```\nOutput:\n```\n");

        // Your code here
        if(N>=4) {
            int temp = arr[1];
            int i;
            for(i = 1; i+2 < N; i+=2){
                int temp2 = arr[i+2];
                arr[i+2] = temp;
                temp = temp2;
            }
            arr[1] = temp;
        }

        for(int i = 0; i<N; i++){
            printf("%d ", arr[i]);
        }
        printf("\n");
        printf("```\n");
    }

    printf("See [%s.cpp](%s.cpp), [%s.in](%s.in) and [%s.output](%s.output).\n", name, name, name, name, name, name);


    return 0;
}