## Array 5. Shift odd-indexed elements right.

Indices are zero-based.

Input format: `N`, followed by the `N` integers. 

Input:
```
3
3 1 2 
```
Output:
```
3 1 2 
```
Input:
```
5
1 2 3 4 9 
```
Output:
```
1 4 3 2 9 
```
Input:
```
6
3 10 -2 3 1 3 
```
Output:
```
3 3 -2 10 1 3 
```
Input:
```
1
8 
```
Output:
```
8 
```
Input:
```
2
1 2 
```
Output:
```
1 2 
```
Input:
```
4
4 3 2 1 
```
Output:
```
4 1 2 3 
```
Input:
```
7
1 2 3 4 5 6 7 
```
Output:
```
1 6 3 2 5 4 7 
```
See [array5.cpp](array5.cpp), [array5.in](array5.in) and [array5.output](array5.output).
