// mat1_readme.cpp
#include <bits/stdc++.h>

using namespace std;

#define MAX_N 1000
#define MAX_M 1000

int main(){
    int N, M;
    char grid[MAX_N][MAX_M];  // Store characters here

    char name[] = "mat1";

    while(scanf("%d %d", &N, &M)!=EOF){
        printf("Input:\n```\n%d %d\n",N,M);
        // Your code here
        for(int i = 0; i<N; i++){
            for(int j = 0; j<M; j++){
                scanf(" %c", &grid[i][j]);
            }
        }
        for(int i = 0; i<N; i++){
            for(int j = 0; j<M; j++){
                printf("%c", grid[i][j]);
            }
            printf("\n");
        }
        printf("```\nOutput:\n```\n");
        for(int i = 0; i<N; i++){
            for(int j = 0; j<M; j++){
                printf("%c", grid[i][j]);
            }
            printf("\n");
        }
        // /*
            // Or
            // for(int i = 0; i<N; i++){
            //     scanf(" %s", grid[i]);
            //     printf("%s\n", grid[i]);
            // }
        // */
        printf("```\n");
    }
    printf("See [%s.cpp](%s.cpp), [%s.in](%s.in) and [%s.output](%s.output).\n", name,name,name,name,name,name);

    return 0;
}