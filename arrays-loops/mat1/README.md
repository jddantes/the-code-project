## Mat 1. Read in and print a character grid.

Input format: `N` and `M`, followed by `N` lines of `M` characters.

*Hint: Remember to preceed `'%c'` with a space when using `scanf` to cleanly process whitespace.*

Input:
```
4 4
asdf
bcde
ghil
zdfj
```
Output:
```
asdf
bcde
ghil
zdfj
```
Input:
```
2 3
***
---
```
Output:
```
***
---
```
See [mat1.cpp](mat1.cpp), [mat1.in](mat1.in) and [mat1.output](mat1.output).
