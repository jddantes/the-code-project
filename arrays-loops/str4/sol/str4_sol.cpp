// str4_sol.cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    string str;
    while(getline(cin, str)){
        // Your code here
        int N = str.size();
        for(int i = 0; i<N/2; i++){
            swap(str[i], str[N-1-i]);
        }

        cout << str << endl;
    }
    return 0;
}