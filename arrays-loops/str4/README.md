## Str 4. Reverse a string.

Input format: a string on its own line.

Output the reversed string.

*Hint: Make use of the swap() function in <utility>.*

*Hint 2: With integer division, N/2 would give you the lower half, whether even or odd.*

Input:
```
reverse this string
```
Output:
```
gnirts siht esrever
```
Input:
```
somestring
```
Output:
```
gnirtsemos
```
Input:
```
1234567890
```
Output:
```
0987654321
```
Input:
```
aaaad
```
Output:
```
daaaa
```
Input:
```
AGTC
```
Output:
```
CTGA
```
Input:
```
eek
```
Output:
```
kee
```
See [str4.cpp](str4.cpp), [str4.in](str4.in) and [str4.output](str4.output).
