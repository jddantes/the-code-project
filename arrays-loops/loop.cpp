// loop.cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    int i;
    for(i = 1; i<=5; i++){
        printf("i=%d. Executing the body.\n", i);
    }
    printf("The final value of i is %d.\n", i);

    return 0;
}