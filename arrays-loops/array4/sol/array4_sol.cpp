// array4_sol.cpp
#include <bits/stdc++.h>

using namespace std;

#define MAX_N 1000

int main(){
    int N;
    int arr[MAX_N];

    while(scanf("%d", &N)!=EOF){
        for(int i = 0; i<N; i++){
            scanf("%d", &arr[i]);
        }

        // Your code here
        int temp = arr[0];
        int i;
        for(i = 2; i<N; i+=2){
            arr[i-2] = arr[i];
        }
        i-=2;
        arr[i] = temp;

        for(int i = 0; i<N; i++){
            printf("%d ", arr[i]);
        }
        printf("\n");
    }


    return 0;
}