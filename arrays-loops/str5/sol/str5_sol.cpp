// str5_sol.cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    string str;

    while(getline(cin, str)){
        // Your code here
        char c = str[0];
        int N = str.size();
        for(int i = 0; i<N-1; i++){
            str[i] = str[i+1];
        }
        str[N-1] = c;

        cout << str << endl;
    }

    return 0;
}