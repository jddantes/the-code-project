// array1_sol.cpp
#include <bits/stdc++.h>

using namespace std;

#define MAX_N 1000

int main(){
    int N;
    int arr[MAX_N];  // Store the integers here

    while(scanf("%d", &N)!=EOF) {
        // Scan N integers into arr
        for(int i = 0; i<N; i++) scanf("%d", &arr[i]);
        // Print N integer  s
        for(int i = 0; i<N; i++) printf("%d ", arr[i]);
        printf("\n");
    }

    return 0;
}