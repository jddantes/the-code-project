// array1_readme.cpp
#include <bits/stdc++.h>

using namespace std;

#define MAX_N 1000

int main(){
    int N;
    int arr[MAX_N];  // Store the integers here

    char name[] = "array1";

    while(scanf("%d", &N)!=EOF) {
        printf("Input:\n```\n%d\n", N);
        // Scan N integers into arr
        for(int i = 0; i<N; i++) scanf("%d", &arr[i]);
        for(int i = 0; i<N; i++){
            printf("%d ", arr[i]);
        }
        printf("\n");
        printf("```\nOutput:\n```\n");
        // Print N integer  s
        for(int i = 0; i<N; i++) printf("%d ", arr[i]);
        printf("\n");
        printf("```\n");
    }

    printf("See [%s.cpp](%s.cpp), [%s.in](%s.in) and [%s.output](%s.output).\n", name,name,name,name,name,name);

    return 0;
}