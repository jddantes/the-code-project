## Mat 6. Rotate clockwise.

Input format: `N`, followed by `N` lines of `N` integers (square matrix). 

Input:
```
3
1 2 3 
4 5 6 
7 8 9 
```
Output:
```
7 4 1 
8 5 2 
9 6 3 
```
Input:
```
2
1 2 
3 4 
```
Output:
```
3 1 
4 2 
```
Input:
```
4
1 2 3 4 
5 6 7 8 
9 10 11 12 
13 14 15 16 
```
Output:
```
13 9 5 1 
14 10 6 2 
15 11 7 3 
16 12 8 4 
```
Input:
```
5
1 2 3 4 5 
6 7 8 9 10 
11 12 13 14 15 
16 17 18 19 20 
21 22 23 24 25 
```
Output:
```
21 16 11 6 1 
22 17 12 7 2 
23 18 13 8 3 
24 19 14 9 4 
25 20 15 10 5 
```
Input:
```
1
1 
```
Output:
```
1 
```
See [mat6.cpp](mat6.cpp), [mat6.in](mat6.in) and [mat6.output](mat6.output).
