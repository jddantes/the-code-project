// array.cpp
#include <bits/stdc++.h>

using namespace std;

int zeroed[10];  // global arrays are zeroed by default

int main(){
    int arr[10];  // uniniitalized, and might contain random data
    int arr_zero[10] = {};  // or zero it this way
    int other[] = {4,8,10};  // size determined from initialization

    for(int i = 0; i<10; i++){
        printf("%d ", zeroed[i]);
    }
    printf("\n");

    for(int i = 0; i<10; i++){
        printf("%d ", arr[i]);
    }
    printf("\n");

    for(int i = 0; i<10; i++){
        printf("%d ", arr_zero[i]);
    }
    printf("\n");

    for(int i = 0; i<3; i++){
        printf("%d ", other[i]);
    }
    printf("\n");

    char grid[3][20] = {"cat", "dog", "giraffe"};  // 3 strings, each up to 20 characters (including the terminating '\0')
    for(int i = 0; i<3; i++){
        for(int j = 0; grid[i][j]; j++){
            printf("%c", grid[i][j]);
        }
        printf("\n");
    }
    // Or simply:
    for(int i = 0; i<3; i++){
        printf("%s\n", grid[i]);
    }

    return 0;
}