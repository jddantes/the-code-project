## Loop 8. Print a checkered/alternating NxM board.

Input:
```
3 2
```
Output:
```
*X
X*
*X
```
Input:
```
4 5
```
Output:
```
*X*X*
X*X*X
*X*X*
X*X*X
```
Input:
```
9 3
```
Output:
```
*X*
X*X
*X*
X*X
*X*
X*X
*X*
X*X
*X*
```
Input:
```
1 8
```
Output:
```
*X*X*X*X
```
Input:
```
2 2
```
Output:
```
*X
X*
```
See [loop8.cpp](loop8.cpp), [loop8.in](loop8.in) and [loop8.output](loop8.output).
