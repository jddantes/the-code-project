## Loop 1. Different variations of looping.

1. Print 1 to 9, using `i=0` and less than for the condition (`<`).

    ```cpp
    for(int i = 0; < ; )
    ```

2. Print 1 to 9, using `i=1` and `<`.
```cpp
for(int i = 1; < ; )
```
3. Print 1 to 9, using `i=4` and `<=`.
```cpp
for(int i = 4; <= ;)
```
4. Print `'*'` 7 times, using `i=0`, `<`, and `i+=5`.
```cpp
for(int i = 0; < ; i+=5) printf("*");
```
5. Print `'*'` 6 times, using `i=1`, `<=`, and `i*=3`.
```cpp
for(int i = 1; <= ; i*=3) printf("*");
```
6. Print 10 to 1, starting at `i=10` and using `i--`.
```cpp
for(int i = 10; ; i--)
```
7. Print 1 to 7, starting at `i=7` and using `i--`.
```cpp
for(int i = 7; ; i--)
```
8. Print `'*'` 8 times, starting at `i=11` and using `i-=2` and `>=`.
```cpp
for(int i = 11; >=; i-=2) printf("*");
```
9. Print `'*'` 7 times, starting at `i=7` and using `i-=3` and `>`.
```cpp
for(int i = 7; > ; i-=3) printf("*");
```
10. Print 5 multiples of 3 starting at 33 (i.e. 33, 36, 39, 42, 45). No `if`-s, use one variable.
```cpp
for( ; ; )
```
11. Same as 10.), but use two variables: `start` and `rep` which is used with `<` for the stopping condition.
```cpp
for(int start = 33, rep=; rep < ;)
```

See [loop1.cpp](loop1.cpp).