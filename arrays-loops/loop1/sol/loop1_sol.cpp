// loop1.cpp
#include <cstdio>

using namespace std;

int main(){
    // When I say "procs" I mean the number of times i (the counter) goes into the loop

    /*
        1. Print 1 to 9.
    */  
    printf("1. ");
    for(int i = 0; i < 9 ; i++) {  // Procs from 0, 1, ..., 7, 8 (9 total)
        printf("%d ", i+1);
    }
    printf("\n");

    /*
        2. Print 1 to 9.
    */
    printf("2. ");
    for(int i = 1; i < 10 ; i++) {  // Procs from 1, 2, ..., 8, 9 (9 total)
        printf("%d ", i);
    }
    printf("\n");

    /*
        3. Print 1 to 9.
    */
    printf("3. ");
    for(int i = 4; i <= 4 + 8 ; i++) {  // Procs from 4, 5, ..., 4+8=12 (9 total)
        printf("%d ", i - 3);
    }
    printf("\n");

    /*
        4. Print '*' 7 times.
    */
    printf("4. ");
    for(int i = 0; i < 7*5; i+=5){  // Procs from 0, 5, 10, 15, 20, 25, 30 (7 total)
        printf("*");
    }
    printf("\n");

    /*
        5. Print '*' 6 times.
    */
    printf("5. ");
    for(int i = 1; i <= 243 ; i*=3) {  // Procs from 1, 3, 9, 27, 81, 243 (6 total)
        printf("*");
    }
    printf("\n");

    /*
        6. Print 10 to 1.
    */
    printf("6. ");
    for(int i = 10; i>0 ; i--) {  // Procs from 10, 9, ..., 2, 1 (10 total)
        printf("%d ", i);
    }
    printf("\n");

    /*
        7. Print 1 to 7.
    */
    printf("7. ");
    for(int i = 7; i>=1; i--) {  // Procs from 7, 6, ..., 2, 1 (7 total)
        printf("%d ", 7-i+1);
    }
    printf("\n");

    /*
        8. Print '*' 8 times.
    */      
    printf("8. ");
    for(int i = 11; i >= 11-((8-1)*2) ; i-=2) {  // Procs from 11, 9, 7, 5, 3, 1, -1, -3 (8 total)
        printf("*");
    }
    printf("\n");

    /*
        9. Print '*' 7 times.
    */ 
    printf("9. ");
    for(int i = 7; i > 7-((7-0)*3) ; i-=3) {  // Procs from 7, 4, 1, -2, -5, -8, -11 [-14 not included, strictly greater than] (7 total)
        printf("*");
    }
    printf("\n");

    /*
        10. Print 5 multiples of 3 starting from 33 (33, 36, 39, 42, 45)
    */
    printf("10. ");
    for(int i = 33; i<=45 ; i+=3) {
        printf("%d ", i);
    }
    printf("\n");

    /*
        11. Same as 10.), but use start=33, rep, and `<`.
    */
    printf("11. ");
    for(int start = 33, rep=0; rep < 5 ; rep++){  // Rep goes from 0 to 4 (5 total)
        printf("%d ", start + rep*3);  
    }
    printf("\n");


    return 0;
}