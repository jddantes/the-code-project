// loop1.cpp
#include <cstdio>

using namespace std;

int main(){
    /*
        1. Print 1 to 9.
    */  
    printf("1. ");
    for(int i = 0; < ; ) {

    }
    printf("\n");

    /*
        2. Print 1 to 9.
    */
    printf("2. ");
    for(int i = 1; < ;) {
        
    }
    printf("\n");

    /*
        3. Print 1 to 9.
    */
    printf("3. ");
    for(int i = 4; <= ;) {

    }
    printf("\n");

    /*
        4. Print '*' 7 times.
    */
    printf("4. ");
    for(int i = 0; < ; i+=5){
        printf("*");
    }
    printf("\n");

    /*
        5. Print '*' 6 times.
    */
    printf("5. ");
    for(int i = 1; <= ; i*=3) {
        printf("*");
    }
    printf("\n");

    /*
        6. Print 10 to 1.
    */
    printf("6. ");
    for(int i = 10; ; i--) {

    }
    printf("\n");

    /*
        7. Print 1 to 7.
    */
    printf("7. ");
    for(int i = 7; ; i--) {

    }
    printf("\n");

    /*
        8. Print '*' 8 times.
    */      
    printf("8. ");
    for(int i = 11; >= ; i-=2) {
        printf("*");
    }
    printf("\n");

    /*
        9. Print '*' 7 times.
    */  
    for(int i = 7; > ; i-=3) {
        printf("*");
    }
    printf("\n");

    /*
        10. Print 5 multiples of 3 starting from 33 (33, 36, 39, 42, 45)
    */
    printf("10. ");
    for( ; ;) {

    }
    printf("\n");

    /*
        11. Same as 10.), but use start=33, and rep.
    */
    for(int start = 33, rep=; rep < ; ){

    }
    printf("\n");


    return 0;
}