#include <bits/stdc++.h>

using namespace std;

int main(){
    int a = 3;
    int b = 10;
    int c = 7;
    
    printf("With if-else, at most one branch is executed:\n");
    if (a > 0) {
        printf("a is %d\n", a);
    } else if (b > 0) {
        printf("b is %d\n", b);
    } else if (c > 0) {
        printf("c is %d\n", c);
    }

    printf("In contrast:\n");
    if (a > 0) {
        printf("a is %d\n", a);
    }
    if (b > 0) { 
        printf("b is %d\n", b);
    }
    if (c > 0) {
        printf("c is %d\n", c);
    }

    int positive = a > 0 ? 1 : 0;
    printf("%d\n", positive);
    cout << (positive ? "Positive!" : "Not positive.") << endl;
    return 0;
}