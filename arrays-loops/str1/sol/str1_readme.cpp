// str1_readme.cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    string str;
    char name[] = "str1";
    while(getline(cin,str)){
        // Your code here
        printf("Input:\n```\n");
        cout << str << endl;
        printf("```\nOutput:\n```\n");
        for(int i = 0; i<str.size(); i++){
            str[i] = toupper(str[i]);
        }

        cout << str << endl;
        printf("```\n");
    }
    printf("See [%s.cpp](%s.cpp), [%s.in](%s.in) and [%s.output](%s.output).\n", name,name,name,name,name,name );

    return 0;
}