// mat3.cpp
#include <bits/stdc++.h>

using namespace std;

#define MAX_N 1000
#define MAX_M 1000

int main(){
    int arr[MAX_N][MAX_M];
    int N, M;

    while(scanf("%d %d", &N, &M)!=EOF){
        for(int i = 0; i<N; i++){
            for(int j = 0; j<M; j++){
                scanf("%d", &arr[i][j]);
            }
        }

        // Your code here

        // Print output
        for(int i = 0; i<N; i++){
            for(int j = 0; j<M; j++){
                printf("%d ", arr[i][j]);
            }
            printf("\n");
        }
    }

}