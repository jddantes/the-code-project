## Mat 3. Horizontal flip.

Input format: `N` and `M`, followed by `N` lines of `M` integers.

*Hint: Make use of the [swap()](http://www.cplusplus.com/reference/utility/swap/?kw=swap) function in `<utility>`.*

*Hint 2: With integer division, `N/2` would give you the lower half, whether even or odd.*

Input:
```
3 3
1 2 3 
4 5 6 
7 8 9 
```
Output:
```
3 2 1 
6 5 4 
9 8 7 
```
Input:
```
4 4
1 2 3 4 
5 6 7 8 
9 10 11 12 
13 14 15 16 
```
Output:
```
4 3 2 1 
8 7 6 5 
12 11 10 9 
16 15 14 13 
```
Input:
```
5 5
1 2 3 4 5 
6 7 8 9 10 
11 12 13 14 15 
16 17 18 19 20 
21 22 23 24 25 
```
Output:
```
5 4 3 2 1 
10 9 8 7 6 
15 14 13 12 11 
20 19 18 17 16 
25 24 23 22 21 
```
Input:
```
2 2
1 2 
3 4 
```
Output:
```
2 1 
4 3 
```
Input:
```
2 4
1 2 3 4 
5 6 7 8 
```
Output:
```
4 3 2 1 
8 7 6 5 
```
Input:
```
1 1
1 
```
Output:
```
1 
```
Input:
```
1 8
1 2 3 4 5 6 7 8 
```
Output:
```
8 7 6 5 4 3 2 1 
```
Input:
```
8 1
1 
2 
3 
4 
5 
6 
7 
8 
```
Output:
```
1 
2 
3 
4 
5 
6 
7 
8 
```
See [mat3.cpp](mat3.cpp), [mat3.in](mat3.in) and [mat3.output](mat3.output).
