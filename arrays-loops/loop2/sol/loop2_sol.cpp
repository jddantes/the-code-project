// loop2_sol.cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    int N,M;

    while(scanf("%d %d", &N, &M)!=EOF){
        // Code here
        for(int i = 0; i<N; i++){
            for(int j = 0; j<M; j++){
                printf("*");
            }
            printf("\n");
        }
    }

    return 0;
}