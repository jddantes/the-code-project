// char-int.cpp
#include <bits/stdc++.h>

using namespace std;

int arr[256];  // zeroed by default

int main(){
    for(int c = 'd'; c <= 'g'; c++){
        printf("%d %c\n", c, c);
    }

    int freq[26] = {};  // smaller size

    string str = "aaagggzz";

    for(int i = 0; str[i]; i++){
        char c = str[i];
        freq[c-'a']++;  // assuming the string is all lowercase
    }

    // Or alternatively
    for(int i = 0; str[i]; i++){
        arr[str[i]]++;
    }

    for(int c = 'a'; c <= 'z'; c++){
        if(freq[c-'a']){
            printf("%c:%d\n", c, freq[c-'a']);
        }
    }

    for(char c = 'a'; c <= 'z'; c++){
        if(arr[c]){
            printf("%c:%d\n", c, arr[c]);
        }
    }

    return 0;
}