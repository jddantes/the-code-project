// mat8_sol.cpp
#include <bits/stdc++.h>

using namespace std;

#define MAX_N 1000
#define MAX_M 1000
#define MAX_O 1000
int arr1[MAX_N][MAX_M];
int arr2[MAX_M][MAX_O];
int arr3[MAX_N][MAX_O];

int main(){

    int N,M,O;
    while(scanf("%d %d %d", &N, &M, &O)!=EOF){
        for(int i = 0; i<N; i++){
            for(int j = 0; j<M; j++){
                scanf("%d", &arr1[i][j]);
            }
        }
        for(int i = 0; i<M; i++){
            for(int j = 0; j<O; j++){
                scanf("%d", &arr2[i][j]);
            }
        }

        // Your code here
        for(int i = 0; i<N; i++){
            for(int k = 0; k<O; k++){
                int sum = 0;
                for(int j = 0; j<M; j++){
                    sum += arr1[i][j] * arr2[j][k];
                }
                arr3[i][k] = sum;
            }
        }

        // Print output
        for(int i = 0; i<N; i++){
            for(int j = 0; j<O; j++){
                printf("%d ", arr3[i][j]);
            }
            printf("\n");
        }
    }

    return 0;
}