// mat8_readme.cpp
#include <bits/stdc++.h>

using namespace std;

#define MAX_N 1000
#define MAX_M 1000
#define MAX_O 1000
int arr1[MAX_N][MAX_M];
int arr2[MAX_M][MAX_O];
int arr3[MAX_N][MAX_O];

int main(){
    char name[] = "mat8";

    int N,M,O;
    while(scanf("%d %d %d", &N, &M, &O)!=EOF){
        printf("Input:\n```\n%d %d %d\n", N,M,O);
        for(int i = 0; i<N; i++){
            for(int j = 0; j<M; j++){
                scanf("%d", &arr1[i][j]);
            }
        }
        for(int i = 0; i<M; i++){
            for(int j = 0; j<O; j++){
                scanf("%d", &arr2[i][j]);
            }
        }
        for(int i = 0; i<N; i++){
            for(int j = 0; j<M; j++){
                printf("%d ", arr1[i][j]);
            }
            printf("\n");
        }
        for(int i = 0; i<M; i++){
            for(int j = 0; j<O; j++){
                printf("%d ", arr2[i][j]);
            }
            printf("\n");
        }
        printf("```\nOutput:\n```\n");

        // Your code here
        for(int i = 0; i<N; i++){
            for(int k = 0; k<O; k++){
                int sum = 0;
                for(int j = 0; j<M; j++){
                    sum += arr1[i][j] * arr2[j][k];
                }
                arr3[i][k] = sum;
            }
        }

        // Print output
        for(int i = 0; i<N; i++){
            for(int j = 0; j<O; j++){
                printf("%d ", arr3[i][j]);
            }
            printf("\n");
        }
        printf("```\n");
    }
    printf("See [%s.cpp](%s.cpp), [%s.in](%s.in) and [%s.output](%s.output)\n", name,name,name,name,name,name );


    return 0;
}