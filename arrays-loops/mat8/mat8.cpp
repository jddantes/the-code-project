// mat8.cpp
#include <bits/stdc++.h>

using namespace std;

#define MAX_N 1000
#define MAX_M 1000
#define MAX_O 1000

int main(){
    int arr1[MAX_N][MAX_M];
    int arr2[MAX_M][MAX_O];
    int arr3[MAX_N][MAX_O];

    int N,M,O;

    while(scanf("%d %d %d", &N, &M, &O)!=EOF){
        for(int i = 0; i<N; i++){
            for(int j = 0; j<M; j++){
                scanf("%d", &arr1[i][j]);
            }
        }
        for(int i = 0; i<M; i++){
            for(int j = 0; j<O; j++){
                scanf("%d", &arr2[i][j]);
            }
        }

        // Your code here

        // Print output
        for(int i = 0; i<N; i++){
            for(int j = 0; j<O; j++){
                printf("%d ", arr3[i][j]);
            }
            printf("\n");
        }
    }

    return 0;
}