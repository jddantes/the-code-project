// array-segfault.cpp
#include <bits/stdc++.h>

using namespace std;

int arr[3];
int main(){
    arr[-200] = 12;  // segmentation fault!
    return 0;
}