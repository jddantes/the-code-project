**TODO**: union-find, segment tree, fenwick tree, rmq (sparse table), lca, djikstra, mst, max flow

## Resources
- [Big-O Cheatsheet](http://bigocheatsheet.com/)
- [A Gentle Introduction to Algorithm Complexity](http://discrete.gr/complexity/) - A long read, but is helpful if you're not familiar with asymptotic analysis/notation (e.g. you don't know what <code>O(n<sup>2</sup>)</code> means).