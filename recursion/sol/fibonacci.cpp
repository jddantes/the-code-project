#include <bits/stdc++.h>

using namespace std;

int fib(int n){
    // Let fib(0) = 0 and fib(1) = 1 be the base cases.
    if(n == 0) return 0;
    if(n == 1) return 1;

    // Fibonacci numbers are defined by the recurrence fib(n) = fib(n-1) + fib(n-2).
    return fib(n-1) + fib(n-2);
}

int main(){
    int n = 11;
    printf("The %d-th Fibonacci number is %d\n",n, fib(n));
    return 0;
}