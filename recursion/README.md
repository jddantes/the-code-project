Recursion simply refers to a [function](../functions) calling itself. The usual examples are factorial and Fibonacci, try implementing them without using loops.

The only things you need are (1) the recursion itself, and (2) the base cases so your code doesn't go on infinitely.

After this, try Googling for other examples, and then head to the [backtracking](../backtracking) folder.