// pointers2.cpp
#include <bits/stdc++.h>

using namespace std;

void printStr(char * str){
    int len = strlen(str);
    printf("Printing %d characters: ", len);
    // Can just be printf("%s"), but doing this for illustration
    for(int i = 0; i<len; i++){
        printf("%c", str[i]);  // Notice the equivalence between pointers and arrays
    }
}

void upperStr(char * str){
    for(int i = 0; str[i]; i++){
        str[i] = toupper(str[i]);
    }
}

// As usual arrays don't have a terminating element (i.e. '\0' for C-strings), we can't use functions like strlen and need to pass the size manually.
void printIntArr(int * arr, int n){
    for(int i = 0; i<n; i++){
        printf("%d ", arr[i]);
    }
    printf("\n");
}

int main(){
    char cstr[100] = "my c-string asdfasdfasdf\n";
    printf("%s", cstr);  // Notice how we don't use &.
    printf("%s", cstr+3);  // As it decomposes into pointers, we can do pointer arithmetic

    char * ptr = cstr;
    printf("%s", ptr);
    printf("%s", ptr+12);  

    printStr(ptr+4);

    // Note the equivalence
    *(ptr+1) = 'Y';
    ptr[3] = 'C';
    printStr(ptr);

    // Modify within a function
    upperStr(ptr);
    printStr(cstr);

    // Also works of ints, or any other array for that matter
    int intArr[] = {1,2,3,10,4,7};
    int numElements = sizeof(intArr)/sizeof(int);
    printIntArr(intArr, numElements);
    printIntArr(intArr+2, numElements-2);
    int * intPtr = intArr;
    printIntArr(intPtr+4, numElements-4);
    char * cptr = (char*)intPtr;  
    cptr += 4;  // chars are 1 byte, ints are 4 bytes
    printIntArr((int*)cptr, numElements-1);    

    return 0;
}