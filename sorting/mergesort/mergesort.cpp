#include <bits/stdc++.h>

using namespace std;

queue<int> mergesort(queue<int> orig){
    int n = orig.size();

    if(n == 1){
        return orig;
    }

    queue<int> left, right;
    for(int i = 0; i<n; i++){
        if(i < n/2){  // Doesn't really matter where, but be sure it splits your array (<= may not split it for n = 2)
            left.push(orig.front());
        } else {
            right.push(orig.front());
        }
        orig.pop();
    }

    queue<int> ans;
    queue<int> left_sorted = mergesort(left);
    queue<int> right_sorted = mergesort(right);

    while(!left_sorted.empty() && !right_sorted.empty()){
        if(left_sorted.front() <= right_sorted.front()){
            ans.push(left_sorted.front());
            left_sorted.pop();
        } else {
            ans.push(right_sorted.front());
            right_sorted.pop();
        }
    }
    // Clear leftovers
    while(!left_sorted.empty()){
        ans.push(left_sorted.front());
        left_sorted.pop();
    }
    while(!right_sorted.empty()){
        ans.push(right_sorted.front());
        right_sorted.pop();
    }

    return ans;
}

void printSorted(queue<int> que){
    while(!que.empty()){
        printf("%d ", que.front());
        que.pop();
    }
    printf("\n");
}

int main(){
    queue<int> que({9,2,1,39,31,4,1,3,0,0,2,2,4,9,5,234,-4,4,5,99});
    // queue<int> que({2,1});
    printSorted(mergesort(que));
    return 0;
}