## Mergesort
`O(n logn)`. See the [GIF](https://upload.wikimedia.org/wikipedia/commons/c/cc/Merge-sort-example-300px.gif).
## Resources
- [Wikipedia](https://en.wikipedia.org/wiki/Merge_sort)
- [GIF (Wikipedia)](https://upload.wikimedia.org/wikipedia/commons/c/cc/Merge-sort-example-300px.gif)