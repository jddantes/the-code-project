## Insertion Sort
`O(n^2)`.

## Resources
- [Wikimedia GIF](https://commons.wikimedia.org/wiki/File:Insertion-sort-example.gif)
- [TutorialsPoint](https://www.tutorialspoint.com/data_structures_algorithms/insertion_sort_algorithm.htm) explanation/illustration