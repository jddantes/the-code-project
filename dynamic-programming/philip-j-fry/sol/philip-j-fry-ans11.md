We don't use up a sphere, so spheres_in_hand isn't decremented.
```cpp
int next_spheres_in_hand = input_sphere[index] + spheres_in_hand;
```
