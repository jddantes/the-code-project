Make use of overlapping subproblems and memoize the states.
```cpp
int f(int index, int spheres_in_hand) {
    /*
        As there are no more further trips, there are zero additional costs to incur.
    */
    if(index == N){
        return 0;
    }

    /*
        Make use of overlapping subproblems and memoize the states.
    */
    if(solved[index][spheres_in_hand]){
        return memo[index][spheres_in_hand];
    }

    // ...

    solved[index][spheres_in_hand] = true;
    memo[index][spheres_in_hand] = best;
    return best;
}
```