`index`
- Just like with knapsack. This tells as where we are, i.e. the current trip.
- `int` from `0` to `N-1`. 

`spheres_in_hand`
- Instead of `weight_in_bag` as in knapsack, we keep track of the current number of magical spheres in hand.
- `int`. As we can use a sphere at most once on a trip, it makes sense to keep only at most `N-1` spheres (second up to the last trip).

