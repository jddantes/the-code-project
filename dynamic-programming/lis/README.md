## Longest Increasing Subsequence (LIS, classic)
Given a list (an array) of numbers, find a subsequence (a subset of the numbers, not necessarily contiguous) such that all the elements are in increasing order. This subsequence must be the longest possible.

Example (from [Geeks for Geeks](http://www.geeksforgeeks.org/dynamic-programming-set-3-longest-increasing-subsequence/)):
```
arr = [3,10,2,1,20]
```
The LIS is 
```
[3,10,20]
```

This can be done in O(n<sup>2</sup>). An O(n log k) solution exists for this problem, but personally may not really help in understanding DP in the backtracking sense.

## Resources
- [Geeks for Geeks](http://www.geeksforgeeks.org/dynamic-programming-set-3-longest-increasing-subsequence/)
- [O(n log k)](https://en.wikipedia.org/wiki/Longest_increasing_subsequence)

## Problems
- [Trainsorting (UVa 11456)](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=2451) - LIS Variant.
- [O(n log k)](https://kth.kattis.com/problems/longincsubseq)

