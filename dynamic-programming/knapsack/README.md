## Knapsack Problem (classic).
Given a list of items each with its own value and weight, which items should you select to give you the maximum total value? Catch&mdash;the total weight of the selected items must not exceed a certain capacity (your "knapsack").

## Resources
- [Geeks for Geeks](http://www.geeksforgeeks.org/dynamic-programming-set-10-0-1-knapsack-problem/)

## Problems
- [Knapsack (Kattis)](https://open.kattis.com/problems/knapsack) - Print solution.
