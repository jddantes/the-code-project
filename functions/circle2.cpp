// circle2.cpp
#include <bits/stdc++.h>

using namespace std;

#define PI 3.14

double get_area(double r){
    return PI*r*r;
}

int main(){
    double r = 2.5;
    double area = get_area(r);
    printf("The area of the circle with radius %f is %f\n", r, area);
    return 0;
}