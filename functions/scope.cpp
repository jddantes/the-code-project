// scope.cpp
#include <bits/stdc++.h>

using namespace std;

string reverse_string(string str){  // The function has its own 'str', separate from the original 'str'. This is also called pass-by-value.
    int n = str.size();
    for(int i = 0; i<n/2; i++){  // Note that this is integer division, so n/2 is floor(n/2).
        swap(str[i], str[n-1-i]);
    }
    return str;
}

// void means the function does not return anything, but merely does stuff.
void reverse_orig_string(string &str){  // The & means pass-by-reference, so we can modify the original 'str'.
    int n = str.size();
    for(int i = 0; i<n/2; i++){
        swap(str[i], str[n-1-i]);
    }
}

void reverse_string_with_ptr(char * str){
    int n = strlen(str);
    for(int i = 0; i<n/2; i++){
        swap(str[i], str[n-1-i]);
    }
}

int main(){
    string str = "asdf";
    cout << "The original str is " << str << endl;
    string rev = reverse_string(str);
    cout << "The reversed is " << rev << endl;
    cout << "str is still " << str << endl;

    reverse_orig_string(str);
    cout << "str is now " << str << endl;

    cout << endl;

    string other_str = "dog";
    cout << "Trying it out on other_str: " << other_str << endl;
    cout << "Reversed is " << reverse_string(other_str) << endl;
    cout << "other_str is still " << other_str << endl;
    reverse_orig_string(other_str);
    cout << "other_str has been modified to " << other_str << endl;

    cout << endl;

    char cstr[100] = "cat";
    printf("Reversing a C-string \"%s\" by passing pointers\n", cstr);
    reverse_string_with_ptr(cstr);
    printf("cstr is now %s\n", cstr);

    return 0;
}
