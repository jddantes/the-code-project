## Functions
[Functions](https://www.tutorialspoint.com/cplusplus/cpp_functions.htm) are things that do things; *they have a function*, whether it's to get the area of a circle, to check whether a string is a palindrome, or to print the letter `'x'` an arbitrary number of times. Just like in math, you can think of it as a black box that takes some input (maybe none), does some stuff, and then outputs (*returns*, *gives back*, *spits out*, or in Flipino, *binabalik*, *nilalabas*, *niluluwa*, whichever gets the point across) something.

Consider the following:
```cpp
// circle.cpp
#include <bits/stdc++.h>

using namespace std;

#define PI 3.14

int main(){
    double r = 2.5;
    double area = PI*r*r;
    printf("The area of the circle with radius %f is %f\n", r, area);
    return 0;
}
```
Output:
```
The area of the circle with radius 2.500000 is 19.625000
```
We can rewrite this using functions, as shown:
```cpp
// circle2.cpp
#include <bits/stdc++.h>

using namespace std;

#define PI 3.14

double get_area(double r){
    return PI*r*r;
}

int main(){
    double r = 2.5;
    double area = get_area(r);
    printf("The area of the circle with radius %f is %f\n", r, area);
    return 0;
}
```
It outputs the same thing:
```
The area of the circle with radius 2.500000 is 19.625000
```
So, why would we want to this? Well, for *reusability* and *modularity*. In our example, we don't need to type out `PI*r*r`, and can reuse it for different variables. This also reduces the risk of errors (e.g. typing `PI*r` instead) and makes our code cleaner and shorter. See the code below.
```cpp
// circle3.cpp
#include <bits/stdc++.h>

using namespace std;

#define PI 3.14

double get_area(double r){
    return PI*r*r;
}

int main(){
    double r = 2.5;
    printf("The area of the circle with radius %f is %f\n", r, get_area(r));

    double another_r = 5;
    printf("The area of another circle with radius %f is %f\n", another_r, get_area(another_r));  // We don't need to type out PI*another_r*another_r, but can just reuse our function.
    return 0;
}
```
Output:
```
The area of the circle with radius 2.500000 is 19.625000
The area of another circle with radius 5.000000 is 78.500000
```
This is further exemplified with more complex functions; say we want to get the distance between two points (represented by `pair<int,int>`, and do so multiple times. Compare this
```cpp
// points.cpp
#include <bits/stdc++.h>

using namespace std;

typedef pair<int, int> pi;

int main(){
    pi pt1(1,3.5), pt2(4,7.5), pt3(10,6.5);  // Three random points

    printf("The distance between pt1 (%.1f,%.1f) and pt2 (%.1f, %.1f) is %f\n", pt1.first, pt1.second, pt2.first, pt2.second, sqrt((pt1.first-pt2.first)*(pt1.first-pt2.first)+(pt1.second-pt2.second)*(pt2.second-pt2.second)));
    printf("The distance between pt1 (%.1f,%.1f) and pt3 (%.1f, %.1f) is %f\n", pt1.first, pt1.second, pt3.first, pt3.second, sqrt((pt1.first-pt3.first)*(pt1.first-pt3.first)+(pt1.second-pt3.second)*(pt3.second-pt3.second)));

    return 0;
}
```
Output:
```
The distance between pt1 (1.0,3.5) and pt2 (4.0,7.5) is 5.000000
The distance between pt1 (1.0,3.5) and pt3 (10.0,6.5) is 9.486833
```
with this
```cpp
// points.cpp
#include <bits/stdc++.h>

using namespace std;

typedef pair<double, double> pd;

double square(double x){
    return x*x;
}

double get_dist(pd a, pd b){
    return sqrt(square(a.first-b.first)+square(a.second-b.second));
}

int main(){
    pd pt1(1,3.5), pt2(4,7.5), pt3(10,6.5);  // Three random points

    printf("The distance between pt1 (%.1f,%.1f) and pt2 (%.1f,%.1f) is %f\n", pt1.first, pt1.second, pt2.first, pt2.second, get_dist(pt1, pt2));
    printf("The distance between pt1 (%.1f,%.1f) and pt3 (%.1f,%.1f) is %f\n", pt1.first, pt1.second, pt3.first, pt3.second, get_dist(pt1, pt3));

    return 0;
}
```
Output:
```
The distance between pt1 (1.0,3.5) and pt2 (4.0,7.5) is 5.000000
The distance between pt1 (1.0,3.5) and pt3 (10.0,6.5) is 9.486833
```
Does the same thing, but much cleaner! Note here that we can use functions within functions. We have defined `square` and used it within `get_dist`, and also have used [sqrt](http://www.cplusplus.com/reference/cmath/sqrt/?kw=sqrt) which is provided for us in `<cmath>`! Also note, `scanf`, `printf`, as well as the others used for [io](/cpp-stl/io) are functions too. `main` is a function as well!

## Scope
When passing parameters to functions, it is essential to talk about [scope](https://www.tutorialspoint.com/cplusplus/cpp_variable_scope.htm). Just like when declaring a block (i.e., `{}` as in `if-else` and in `loops`), a function will have its own scope (i.e., own set of variables). 

Consider the following code:
```cpp
// scope.cpp
#include <bits/stdc++.h>

using namespace std;

string reverse_string(string str){  // The function has its own 'str', separate from the original 'str'. This is also called pass-by-value.
    int n = str.size();
    for(int i = 0; i<n/2; i++){  // Note that this is integer division, so n/2 is floor(n/2).
        swap(str[i], str[n-1-i]);
    }
    return str;
}

// void means the function does not return anything, but merely does stuff.
void reverse_orig_string(string &str){  // The & means pass-by-reference, so we can modify the original 'str'.
    int n = str.size();
    for(int i = 0; i<n/2; i++){
        swap(str[i], str[n-1-i]);
    }
}

void reverse_string_with_ptr(char * str){
    int n = strlen(str);
    for(int i = 0; i<n/2; i++){
        swap(str[i], str[n-1-i]);
    }
}

int main(){
    string str = "asdf";
    cout << "The original str is " << str << endl;
    string rev = reverse_string(str);
    cout << "The reversed is " << rev << endl;
    cout << "str is still " << str << endl;

    reverse_orig_string(str);
    cout << "str is now " << str << endl;

    cout << endl;

    string other_str = "dog";
    cout << "Trying it out on other_str: " << other_str << endl;
    cout << "Reversed is " << reverse_string(other_str) << endl;
    cout << "other_str is still " << other_str << endl;
    reverse_orig_string(other_str);
    cout << "other_str has been modified to " << other_str << endl;

    cout << endl;

    char cstr[100] = "cat";
    printf("Reversing a C-string \"%s\" by passing pointers\n", cstr);
    reverse_string_with_ptr(cstr);
    printf("cstr is now %s\n", cstr);

    return 0;
}
```
Output:
```
The original str is asdf
The reversed is fdsa
str is still asdf
str is now fdsa

Trying it out on other_str: dog
Reversed is god
other_str is still dog
other_str has been modified to god

Reversing a C-string "cat" by passing pointers
cstr is now tac
```
In `reverse_string`, we define a new variable `string str` inside the function that is a copy of the passed parameter (pass-by-value). In contrast, in `reverse_orig_string`, we indicate that we want to modify the original variable by defining it as `string &str` (pass-by-reference). We can also do this using [pointers](/pointers), but the details are another topic altogether. See [here](http://www.cplusplus.com/articles/z6vU7k9E/) for further reading.

Finally, note that some languages may have different scoping rules. For example, in Python, passing an object (like a list) is essentially passing the original object; editing it inside the function is the same as editing the original one. See the discussion [here](http://stackoverflow.com/questions/986006/how-do-i-pass-a-variable-by-reference).

There are also other examples in [loops](/loops), so try going through them.