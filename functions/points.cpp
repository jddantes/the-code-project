// points.cpp
#include <bits/stdc++.h>

using namespace std;

typedef pair<double, double> pd;

int main(){
    pd pt1(1,3.5), pt2(4,7.5), pt3(10,6.5);  // Three random points

    printf("The distance between pt1 (%.1f,%.1f) and pt2 (%.1f,%.1f) is %f\n", pt1.first, pt1.second, pt2.first, pt2.second, sqrt((pt1.first-pt2.first)*(pt1.first-pt2.first)+(pt1.second-pt2.second)*(pt1.second-pt2.second)));
    printf("The distance between pt1 (%.1f,%.1f) and pt3 (%.1f,%.1f) is %f\n", pt1.first, pt1.second, pt3.first, pt3.second, sqrt((pt1.first-pt3.first)*(pt1.first-pt3.first)+(pt1.second-pt3.second)*(pt1.second-pt3.second)));

    return 0;
}