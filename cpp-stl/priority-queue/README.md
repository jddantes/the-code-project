## Priority Queue

With priority queues, we also have `push` and `pop` as with [queues](../queue), with the difference being that what we pop is the element with *highest priority*. For C++, this would be the largest element, but with Java it's the opposite. Just like with [stacks](../stack), we can use [top](http://www.cplusplus.com/reference/queue/priority_queue/top/) to look at the element that would be popped (does not remove the element). As priority queues are implemented as heaps (see links below), operations take `O(log n)`.

See [priority_queue.cpp](priority_queue.cpp).
```cpp
// priority_queue.cpp
#include <iostream>
#include <queue>

using namespace std;

int main(){
    priority_queue<int> pq;  // [], empty
    pq.push(5);  // [5]
    pq.push(1);  // [5,1]
    pq.push(12);  // [12,5,1]; note that the highest element gets in front of the queue
    pq.push(-3);  // [12,5,1,-3]
    pq.push(17);  // [17,12,5,1,-3]

    while(!pq.empty()){
        int item = pq.top();  // Does not pop it 
        pq.pop();
        cout << item << " ";
    }
    cout << endl;

    

    return 0;
}
```
Output:
```
17 12 5 1 -3 
```

## Resources
- [cplusplus.com](http://www.cplusplus.com/reference/queue/priority_queue/?kw=priority_queue)
- [Quora](https://www.quora.com/Why-is-the-complexity-of-pop-function-of-a-standard-C++-priority-queue-O-log-n). Talks about complexity.
- [Heaps](http://www.eecs.wsu.edu/~ananth/CptS223/Lectures/heaps.pdf). Also [here](http://pages.cs.wisc.edu/~vernon/cs367/notes/11.PRIORITY-Q.html), and more reading [here](http://algs4.cs.princeton.edu/24pq/).