To get started, go to the [i/o](io) folder. If you're already familiar, just go through the different data structures available with STL. Suggested order: [vectors](vector), [queues](queue), and [stacks](stack). [Priority queue](priority-queue), then [sets](set) and [maps](map). I rarely use [deques](deque) and [lists](list), but they're there for reference. I've also included some code on [strings](strings) if you're not familiar with them yet.

## Installation

C and C++ are **compiled** languages, meaning you compile your source code into a separate executable which is what you then run.

Contrast this with **interpreted** languages like Python, where you can simply run your code as in 
~~~
python mycode.py 
~~~

To check if you have gcc (C compiler) or g++ (C++), simply type them in the terminal:
```
gcc -v
```
```
g++ -v
```

Linux/Unix systems usually have these by default. For Windows, install them using [MinGW](http://www.mingw.org/).

Compile your code like so:
```
g++ mycode.cpp
```
which will make an executable file `a.out` (or `a.exe` I think for Windows) which you can then run:
```
./a.out
```

To use an executable name other than `a.out`, use the `-o` flag:
```
g++ mycode.cpp -o myexecutable.o
```
and then run it as before:
```
./myexecutable.o
```

Also, you might want to check if you can use C++11. The recent version uses C++14 so you might not need additional flags, but otherwise try compiling it like this:
```
g++ mycode.cpp -o myexecutable.o --std=c++11
```
C++11 provides some useful features, most notably the use of the `auto` keyword, to be discussed in the sample code.

## Tips

**Include everything with `<bits/stdc++.h>`.**

When developing big projects you usually don't want to pollute the namespace like this, but for repetitive coding (and also in competitive programming environments) this is usually convenient.
```cpp
#include <bits/stdc++.h>
```

**Use typedefs.**

Typedefs save you time and effort by renaming/aliasing different variable types:
```cpp
#include <bits/stdc++.h>

using namespace std;

typedef long long ll;
typedef pair<ll, ll> pl;

int main(){
    long long a = 1;
    ll b = 2;  // same as long long b
    pl p(a,b);  // same as pair<long long, long long> p(a,b);
    printf("%lld %lld\n", p.first, p.second);
    return 0;
}
```

**Use the `auto` keyword.**
This feature will save you coding time and make it easier to change code. For example:
```cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    map<pair<string,int>, pair<int, pair<int,int>>> mymap;  // Three ints
    
    for(map<pair<string,int>, pair<int, pair<int,int>>>::iterator itr = mymap.begin(); itr!=mymap.end(); ++itr){
        // Print
    }

    map<pair<string,int>, pair<int, pair<int,int>>>::iterator itr = mymap.find(pair<string,int>("sample",1));
}
```

Then you find out that you need to change the map values to contain four `int` values, so you will change **all** the instances of `map<pair<string,int>, pair<int, pair<int,int>>>` to `map<pair<string,int>, pair<pair<int,int>, pair<int,int>>>`:
```cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    map<pair<string,int>, pair<pair<int,int>, pair<int,int>>> mymap;  // If we change this...
    
    for(map<pair<string,int>, pair<pair<int,int>, pair<int,int>>>::iterator itr = mymap.begin(); itr!=mymap.end(); ++itr){  // ...we need to update this...
        // Print
    }

    map<pair<string,int>, pair<pair<int,int>, pair<int,int>>>::iterator itr = mymap.find(pair<string,int>("sample",1));  // ...and this as well, and possibly more lines for bigger code!
}
```
In contrast, when we have the `auto` keyword, our code from before that looks like this:
```cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    map<pair<string,int>, pair<int, pair<int,int>>> mymap;  // If we change this...
    
    for(auto el : mymap){
        // Print
    }

    auto itr = mymap.find(pair<string,int>("sample",1));
}
```
when changed to four integers, simply just becomes:
```cpp
#include <bits/stdc++.h>

using namespace std;

int main(){
    map<pair<string,int>, pair<pair<int,int>, pair<int,int>>> mymap;  // ... we only changed this!
    
    for(auto el : mymap){
        // Print
    }

    auto itr = mymap.find(pair<string,int>("sample",1));
}
```
Nothing much has changed!

Also, another detail: declarations like this
```cpp
vector<vector<int>> arr;
```
work in C++11 and above, but need to have spaces in older versions:
```cpp
vector<vector<int> > arr;
```

## Resources
- [cplusplus.com](http://cplusplus.com). Tutorials, reference, and STL API are all here. Do a quick search if you want do learn about something (e.g. deques, lists, unordered maps).
- [Bo Qian's Youtube playlist for STL](https://www.youtube.com/watch?v=Vc1RyqWFbiA&list=PL5jc9xFGsL8G3y3ywuFSvOuNm3GjBwdkb). Check this out for an overview on STL, iterators, etc.


