## Stack
A stack is... a stack. You put things on top, and can only remove from the top and not from the bottom or anywhere else. Think Pringles! This is also the reason why it's called **LIFO (Last In, First Out)**.

As such, you only have two operations: `push` and `pop`. Here, `push` places an item at the top of the stack, whereas `pop` removes the item at the top. Just like with the [queue](../queue), this is done in `O(1)` time.

For C++, we have [top](http://www.cplusplus.com/reference/stack/stack/top/) which gets the item at the top of the stack (it does not remove it), as well as [empty](http://www.cplusplus.com/reference/stack/stack/empty/) and [size](http://www.cplusplus.com/reference/stack/stack/size/).

See [stack.cpp](stack.cpp).
```cpp
// stack.cpp
#include <stack>
#include <iostream>

using namespace std;

int main(){
    stack<int> st;  // []; empty stack

    st.push(5);  // [5]
    st.push(17);  // [17,5]
    st.push(16);  // [16,17,5]
    st.push(35);  // [35,16,17,5]
    st.push(3);  // [2,35,16,17,5]

    cout << "The stack has " << st.size() << " items." << endl;  // 5 elements
    int item = st.top();  // 2; does not remove from the stack
    cout << "The item at the top is " << item << endl;
    cout << "There are still " << st.size() << " items in the stack" << endl;  // still 5 elements

    st.pop();  // [35,16,17,5]
    cout << "The top of the stack is now " << st.top() << endl;  // 35
    cout << "The stack now has " <<  st.size() << " elements" << endl;  // 4 elements
    
    st.push(41);  // [41,35,16,17,5]
    cout << "Printing:";
    while(!st.empty()){
        cout << " " << st.top();
        st.pop();
    }
    cout << endl;
    cout << "The stack is now empty with " << st.size() << " elements" << endl;

    return 0;
}
```

Output:
```cpp
The stack has 5 items.
The item at the top is 3
There are still 5 items in the stack
The top of the stack is now 35
The stack now has 4 elements
Printing: 41 35 16 17 5
The stack is now empty with 0 elements
```
## References
- [cplusplus.com](http://www.cplusplus.com/reference/stack/stack/)

## Additional Practice
For extra practice, try to implement a stack on your own (just use an array).

For practice on usage, try Ch. 2 of uHunt. Also try [backtracking exercise 4](/backtracking/backtracking4) (my implementation uses stacks), and study some graph algos like finding [strongly connected components (SCC's)](/competitive-programming/graphs/scc).

## Problems
- [Parentheses Balance (UVa 673)](https://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=24&page=show_problem&problem=614). Classic bracket matching.