## Deque
Deques (**D**ouble-**E**nded **QUE**ues) extend [queues](../queue) by also allowing `push`/`pop` at the [front](http://www.cplusplus.com/reference/deque/deque/front/). Thus, we have [push_front](http://www.cplusplus.com/reference/deque/deque/push_front/) and [pop_front](http://www.cplusplus.com/reference/deque/deque/pop_front/), in addition to [push_back](http://www.cplusplus.com/reference/deque/deque/push_back/), [pop_back](http://www.cplusplus.com/reference/deque/deque/pop_back/) and [back](http://www.cplusplus.com/reference/deque/deque/back/).

We can also access elements using `[]`, just like with [vectors](../vector). All these operations are supposedly done in `O(1)` time, although in my experience deques can be slower then vectors (especially when using `[]`, as deques may not be contiguous in memory). However, I did not test this rigorously and my friend uses it a lot, so perhaps just see how it works for you. Also try searching about it; I've listed some links below.

See [deque.cpp](deque.cpp).
```cpp
// deque.cpp
#include <iostream>
#include <deque>

using namespace std;

int main(){
    deque<int> dq;  // []; empty
    dq.push_back(1); // [1]
    dq.push_back(7);  // [1,7]
    dq.push_back(4);  // [1,7,4]
    for(int i = 0; i<dq.size(); i++){
        cout << dq[i] << " ";
    }
    cout << endl;

    dq.push_front(2);  // [2,1,7,4]
    dq.push_front(5);  // [5,2,1,7,4]
    for(int i = 0; i<dq.size(); i++){
        cout << dq[i] << " ";
    }
    cout << endl;

    dq.pop_front();  // [2,1,7,4]
    dq.pop_back();  // [2,1,7]
    for(int i = 0; i<dq.size(); i++){
        cout << dq[i] << " ";
    }
    cout << endl;

    return 0;
}
```
Output:
```
1 7 4 
5 2 1 7 4 
2 1 7 
```

## References]
- [cplusplus.com](http://www.cplusplus.com/reference/deque/deque/?kw=deque)
- [StackOverflow](http://stackoverflow.com/questions/5345152/why-would-i-prefer-using-vector-to-deque). Also links to [this](http://www.gotw.ca/gotw/054.htm).
- [StackOverflow](http://stackoverflow.com/questions/13758509/why-is-stdvector-so-much-more-popular-than-stddeque) again.
- [Benchmarked (plotted graphs) by someone](http://baptiste-wicht.com/posts/2012/12/cpp-benchmark-vector-list-deque.html)