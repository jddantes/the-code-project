// set.cpp
#include <iostream>
#include <set>

using namespace std;

int main(){
    set<int> s({7,1,9});  // {1,7,9}; constructor using initializer list; note that elements are sorted

    for(set<int>::iterator itr = s.begin(); itr!=s.end(); itr++){
        cout << *itr << " ";
    }
    cout << endl;

    s.insert(7);  // {1,7,9}  // elements are unique
    s.insert(8);  // {1,7,8,9}
    for(set<int>::iterator itr = s.begin(); itr!=s.end(); itr++){
        cout << *itr << " ";
    }
    cout << endl;

    s.erase(9);  // {1,7,8}
    for(set<int>::iterator itr = s.begin(); itr!=s.end(); itr++){
        cout << *itr << " ";
    }
    cout << endl;

    // Check element using count()
    if(s.count(7)){
        cout << "7 is in the set" << endl;
    }

    // Check element using iterators and find()
    if(s.find(6) == s.end()){
        cout << "6 is not in the set" << endl;
    }

    // Which item is at least as large as 5?
    set<int>::iterator b_srch = s.lower_bound(5);
    cout << *b_srch << endl;  // 7

    // Which item is at least as large as 7?
    b_srch = s.lower_bound(7);
    cout << *b_srch << endl;  // 7

    // Which item is STRICTLY greater than 7?
    b_srch = s.upper_bound(7);
    cout << *b_srch << endl;  // 8

    return 0;
}