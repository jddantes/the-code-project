## Set
Just like in math, a set is a (unique) collection of elements, like `{1,5,7}`. For C++, a set is automatically sorted, although there are also [unordered_sets](http://www.cplusplus.com/reference/unordered_set/unordered_set/?kw=unordered_set) which may be desirable for some cases (benefits on speed).

We use [insert](http://www.cplusplus.com/reference/set/set/insert/) to add elements, and [erase](http://www.cplusplus.com/reference/set/set/erase/) to remove them. [Iterators](http://www.cplusplus.com/reference/iterator/) are used to traverse the set, and also to check if an element exists using [find](http://www.cplusplus.com/reference/set/set/find/). Alternatively, [count](http://www.cplusplus.com/reference/set/set/count/) can also be used to check if an element is in the set. As `set` maintains an ordered list (binary tree implementation), these operations are done in `O(log n)`.

Additionally, [lower_bound](http://www.cplusplus.com/reference/set/set/lower_bound/)/[upper_bound](http://www.cplusplus.com/reference/set/set/lower_bound/) have been provided for easy binary searching.

See [set.cpp](set.cpp).
```cpp
// set.cpp
#include <iostream>
#include <set>

using namespace std;

int main(){
    set<int> s({7,1,9});  // {1,7,9}; constructor using initializer list; note that elements are sorted

    for(set<int>::iterator itr = s.begin(); itr!=s.end(); itr++){
        cout << *itr << " ";
    }
    cout << endl;

    s.insert(7);  // {1,7,9}  // elements are unique
    s.insert(8);  // {1,7,8,9}
    for(set<int>::iterator itr = s.begin(); itr!=s.end(); itr++){
        cout << *itr << " ";
    }
    cout << endl;

    s.erase(9);  // {1,7,8}
    for(set<int>::iterator itr = s.begin(); itr!=s.end(); itr++){
        cout << *itr << " ";
    }
    cout << endl;

    // Check element using count()
    if(s.count(7)){
        cout << "7 is in the set" << endl;
    }

    // Check element using iterators and find()
    if(s.find(6) == s.end()){
        cout << "6 is not in the set" << endl;
    }

    // Which item is at least as large as 5?
    set<int>::iterator b_srch = s.lower_bound(5);
    cout << *b_srch << endl;  // 7

    // Which item is at least as large as 7?
    b_srch = s.lower_bound(7);
    cout << *b_srch << endl;  // 7

    // Which item is STRICTLY greater than 7?
    b_srch = s.upper_bound(7);
    cout << *b_srch << endl;  // 8

    return 0;
}
```
Output:
```
1 7 9 
1 7 8 9 
1 7 8 
7 is in the set
6 is not in the set
7
7
8
```
We can also use the `auto` keyword:
```cpp
// set_cpp11.cpp
#include <iostream>
#include <set>

using namespace std;

int main(){
    set<int> s({1,3,5});

    for(auto itr = s.begin(); itr!=s.end(); itr++){
        cout << *itr << " ";
    }
    cout << endl;

    for(auto el : s){
        cout << el << " ";
    }
    cout << endl;

    // Erase an element using iterators
    auto it = s.find(3);
    if(it != s.end()){  // element exists
        s.erase(it);
    }
    // {1,5}
    for(auto el : s){
        cout << el << " ";
    }
    cout << endl;


    return 0;
}
```
Output:
```
1 3 5 
1 3 5 
1 5 
```

If we need to, we can maintain a non-unique collection of elements using [multiset]((http://www.cplusplus.com/reference/set/multiset/?kw=multiset)).

## References
- [Set](http://www.cplusplus.com/reference/set/set/?kw=set)
- [Unordered set (C++11)](http://www.cplusplus.com/reference/unordered_set/unordered_set/?kw=unordered_set)
- [Multiset](http://www.cplusplus.com/reference/set/multiset/?kw=multiset)
- [Unordered multiset (C++11)](http://www.cplusplus.com/reference/unordered_set/unordered_multiset/)