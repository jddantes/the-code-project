// map.cpp
#include <iostream>
#include <map>

using namespace std;

int main(){
    map<string, int> freq;  // Let's say we want to keep track of the number of animals we see.
    freq["dog"] = 5;
    freq["cat"] = 3;
    freq["giraffe"] = 2;

    cout << "We have " << freq.size() << " entries" << endl;
    for(map<string,int>::iterator itr = freq.begin(); itr!=freq.end(); itr++){  // Notice how we type so much stuff because of iterators; this is so much cleaner if we use 'auto'
        cout << itr->first << ":" << itr->second << endl;  // key-value pairs are automatically sorted
    }

    cout << freq["not found"] << endl;  // If we try to access elements that are not in the map, a key-value pair is inserted (the default constructors are used)
    cout << "We now have " << freq.size() << " entries because of that" << endl;
    for(map<string,int>::iterator itr = freq.begin(); itr!=freq.end(); itr++){ 
        cout << itr->first << ":" << itr->second << endl;
    }

    // A cleaner way is to use find(), which does not insert a new element 
    if(freq.find("owl") == freq.end()){  // Not found
        cout << "We did not see any owls" << endl;
    }
    cout << "Freq size is still " << freq.size() << endl;

    return 0;
}