// map_cpp11.cpp
#include <iostream>
#include <unordered_map>

using namespace std;

int main(){
    unordered_map<string, int> freq;  // Not sorted, uses hashes to be faster
    freq["dog"] = 5;
    freq["cat"] = 3;
    freq["giraffe"] = 2;

    for(auto kv : freq){  // Much cleaner, and we don't need to change anything if we change the map type.
        cout << kv.first << ":" << kv.second << endl;
    }
    return 0;
}