## Queue
Think of the [queue](https://en.wikipedia.org/wiki/Queue_(abstract_data_type)) at the cafeteria. The first person in line gets served, followed by the second, then the third, and so on... (well ideally). Latecomers can only line up at the end of the queue, and not cut in the middle or worse in front of the queue (again, ideally). Hence, queues are also referred to as **FIFO (First In, First Out)**.

Thus, you have two operations: `pop`, or remove (process) the element at the front, and `push`, or add (append) an element to the queue. This is conveniently done in `O(1)` time, similar to a [stack](../stack).

For C++, a separate method [front](http://www.cplusplus.com/reference/queue/queue/front/) is used to just look at the front of the queue (it does not remove it yet). There are also other helper functions, like [empty](http://www.cplusplus.com/reference/queue/queue/empty/) and [size](http://www.cplusplus.com/reference/queue/queue/size/).

See [queue.cpp](queue.cpp).
```cpp
// queue.cpp
#include <queue>
#include <iostream>

using namespace std;

int main(){
    queue<int> que;  // [] ; queue is empty

    que.push(1);  // [1]
    que.push(10);  // [1,10]
    que.push(5);  // [1,10,5]
    que.push(6);  // [1,10,5,6]

    cout << "Size is " << que.size() << endl;  // 4 elements in queue
    
    int item = que.front();  // gets the item at the front of the queue, which is 1
    cout << "Front: " << item << endl;
    cout << "Size is " << que.size() << endl;  // Calling front() doesn't remove the item at the front...

    que.pop();  // ...but pop() does; [10,5,6]
    cout << "Size after popping is  " << que.size() << endl;  // Now only 3 elements
    cout << "Front is now " << que.front() << endl;  // 10

    cout << "Going through the queue..." << endl;
    while(!que.empty()){
        int item = que.front();
        que.pop();
        cout << "Popped " << item << endl;
    }

    cout << "Queue size is now " << que.size() << endl;


    return 0;
}
```

Output:
```
Size is 4
Front: 1
Size is 4
Size after popping is  3
Front is now 10
Going through the queue...
Popped 10
Popped 5
Popped 6
Queue size is now 0
```

## References
- [cplusplus.com](http://www.cplusplus.com/reference/queue/queue/?kw=queue)

## Additional Practice
To use the built-in `queue`, perhaps try some of the problems in Ch. 2 of uHunt. Also try implementing [Breadth-First Search (BFS)](/competitive-programming/graphs/bfs) in Ch. 4, as it is one *very common* use case for queues.

To improve your coding skill, try implementing a queue on your own too, using just an array. You'll only need two variables, `head` and `tail`. (*Hint: Google "circular queues".*)
